VanillaPlus - ToolImprovements
A21 - Version 1.1.0


______________________________________________________________________________________________________________
***Requires VP-1Core, otherwise known as Vanilla Plus Core***


______________________________________________________________________________________________________________
MAIN Features:
-Tier 1 tools, untouched, Tier 2 tools, slighty faster, Tier 3 tools, slightly more faster.
-Chainsaws now butcher harvest 70% resources like the axes.


______________________________________________________________________________________________________________
If you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
	-All questions welcome from newcombers to seasoned 7d2d people.
	
	
______________________________________________________________________________________________________________
More Details about the author and other sites described on the README in the CORE modlet.
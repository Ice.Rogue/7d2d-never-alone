Vanilla Plus - DecorationBlock
A21 - Version 2.0.4
Description and Updates



***This is a standalone mod, works well with vanilla plus.***


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	-DecorationBlock is SAFE to install on new or existing games.
	-DecorationBlock is DANGEROUS to remove from an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	-Craft at a workbench.
	-Levels in perkAdvancedEngineering slightly reduces ingredient costs.
	-If the block is normally a container, it is secured in its deco version size (10 x 10)
	-Strong as Wood Blocks (500hp), but high explosive resistance.
	-When a player breaks one, it returns to their inventory.
	-When blocks break to falls, zombies, or explosions, high chance of picking it up shortly after.
	-HIGH COMPATIBILITY as it only adds its own code and NOT dependent on existing ones.
	-If it is a light, it can be powered with electricity.
	
	FUTURE EDITS:
	-Better Naming (Localization)
	-Structural deco blocks to be upgradeable to a stronger variant (jail walls, chain link fences, etc)

______________________________________________________________________________________________________________________
5.  CHANGELOG
v2.0.4
-When used standalone, the loot size is too large... so added the needed windows.xml codes to make it work.

v2.0.3
-Some blocks could not be rotated in all directions, now they should.


v2.0.2
-Added localization name for the variant helper and description.


v2.0.1
-Updated to A21! :)
-README updated to new format.


	

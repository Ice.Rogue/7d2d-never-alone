VanillaPlus - VehicleUseLessGas
A21 - Version 1.0.0



***This mod works standalone and is part of the Vanilla Plus Package***


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	
	-VehicleUseLessGas is SAFE to install on new game or existing game.
	-VehicleUseLessGas is SAFE to remove from an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	-Keeping fuel tanks at default size, making the gas last for larger distances (without vehicle mods):
		Gyrocopter: 32km instead of 12km
		Minibike: 32km instead of 16km
		Motorcycle: 48km instead of 24km
		4x4Truck: 60km instead of 40km
	-This mod does not increase fuel tank size, just reduces how much gas is used to travel these distances


______________________________________________________________________________________________________________________
5.  CHANGELOG

v1.0.0
-Created this mod



	

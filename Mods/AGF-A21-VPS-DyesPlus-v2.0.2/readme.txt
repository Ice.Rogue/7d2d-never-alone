VanillaPlus - DyesPlus
A21 - Version 2.0.2


______________________________________________________________________________________________________________
***Works standalone and is part of Vanilla Plus***


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	
	-DyesPlus is SAFE to install on new game or existing game.
	-DyesPlus is DANGEROUS to remove from an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	-Easily interchange dye colors:  A.SCRAP any dye to get 15 paint.  B.CRAFT any dye with 15 paint.
	-Added 28 Colors with simple naming scheme.
	-Added Invisible Dye that can hide a worn item.


______________________________________________________________________________________________________________________
5.  CHANGELOG

v2.0.2
-DISCOVERED BUG: IF you apply the invisible dye, do it from within the inventory and not while wearing, so that buffs from mods apply correct.
-Updated ReadMe File to my new format.
-removed an accidental pasted in line.
-Newer dyes were changing skin color instead of clothes for a few items, now fixed.
-All dyes show up under "science" crafting category.
	

VanillaPlus - AutomobilesRespawn
A21 - Version 1.0.3


______________________________________________________________________________________________________________
***Works standalone and is part of Vanilla Plus***


______________________________________________________________________________________________________________
MAIN Features:
-Vehicles that are salvaged leave an upright car seat to visualize where it will respawn.
	Destroy the upright seat and it will not respawn.
	An on screen tip will appear after salvaging to inform players that it will respawn.
	Respawn set at 10 real life hours. Editable on the blocks.xml.
	

______________________________________________________________________________________________________________
If you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
	-All questions welcome from newcomers to seasoned 7d2d people.
	
	
______________________________________________________________________________________________________________
More Details about the author and other sites described on the README in the CORE modlet.
HUDPlus Addon
A21 - Version 1.5.1


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie
		

______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-REQUIRES HUDPlus main file, as this addon just "adds" onto the original.
	-REQUIRES EAC to be turned off.
	-To work multiplayer, host and clients must have this installed.
	-All 14 languages supported
	-ADDON for HUDPLus is SAFE to install on new or existing games.
	-ADDON for HUDPLus is SAFE to remove from an existing game.


_____________________________________________________________________________________________________________________
4.  FEATURES

FIRST, AGF is NOT the author of the .dll component of this file, only the display on the HUD.
	You can find more information about the .dll here: https://github.com/s7092910/PlayerStatController

Food and Water displays both amount and percentages.
XP to Next Level and Lootstage is displayed.
On the chat output you can see zombie kills, player deaths, and pvp kills.

	
______________________________________________________________________________________________________________
5.  Change Log

v1.5.1
-Updated the .dll used and provided it's source in the readme.

﻿Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,spanish,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese
goZombieMove,UI,Menu,x,,"Zombie Speed",,,,,,,,,,,,,
goZombieMoveDesc,UI,Menu,x,, "Sets zombie movement mode. Balanced around Jog speed",,,,,,,,,,,,,
goDeathPenaltyDesc,UI,Menu,x,, "The mod is designed to be played on PermaDeath, but the option is here for those that wish, to respawn injured.",,,,,,,,,,,,,
perkPackMuleRank1LongDesc,progression,perk Str,,, "Who has time to organize their stuff perfectly? Carry seven more items without being encumbered. (7)",,,,,,,,,,,,,
perkPackMuleRank2LongDesc,progression,perk Str,,, "You know it's in there somewhere. Carry an additional seven more items without being encumbered. (14)",,,,,,,,,,,,,
perkPackMuleRank3LongDesc,progression,perk Str,,, "You know exactly where everything is. Carry seven additional items without being encumbered. (21)",,,,,,,,,,,,,
perkPackMuleRank4LongDesc,progression,perk Str,,, "You just passed inspection, private. Carry seven more items without being encumbered. (28)",,,,,,,,,,,,,
perkPackMuleRank5LongDesc,progression,perk Str,,, "Now you are just showing off, you are either part mule or a strongman. Carry seven more items without being encumbered. (35)",,,,,,,,,,,,,
resourceRepairKitGun,items,Item,,, "Gun Repair Kit",,,,,,,,,,,,,
resourceRepairKitGunDesc,items,Item,,, "The needed materials to get any gun back in working order",,,,,,,,,,,,,
triggerAssembly,items,Item,,, "Trigger Assembly",,,,,,,,,,,,,
triggerAssemblyDesc,items,Item,,, "The trigger assembly needed to make a pipe weapon actually fire",,,,,,,,,,,,,
buffCampfireAOEEffectHealName,buffs,Buff,,, "Healing Comfort",,,,,,,,,,,,,
buffCampfireAOEEffectHealDesc,buffs,Buff,,, "The healing comfort of the fire is warming your soul and your body is mending slightly in its presence.",,,,,,,,,,,,,
foodCanCatfood,items,Food,,, "Can of Kualija Food",,,,,,,,,,,,,
foodCanCatfoodDesc,items,Food,,, "A can of Kualija Chow is a delicacy. For cats. You are not eating this, are you?",,,,,,,,,,,,,
foodCanDogfood,items,Food,,, "Can of Good Boy Dog Food",,,,,,,,,,,,,
foodCanDogfoodDesc,items,Food,,, "A can of dog food. But you are going to eat it because you are a Good Boy, aren't you? Yes you are! You are a Good Boy!",,,,,,,,,,,,,
glassRemovalKit,items,Item,,, "Glass Removal Kit",,,,,,,,,,,,,
glassRemovalKitDesc,items,Item,,, "A pre-made kit containing tweezers and bandage to remove glass and stop any bleeding.",,,,,,,,,,,,,


statCureInfection,ui_display,Item stat,,, "Reduce Infection Symptoms",,,,,,,,,,,,,


buffGlassEmbededName,,,,, "Embedded Glass",,,,,,,,,,,,,
buffGlassEmbededDesc,,,,, "You have glass embedded into your body. You need to use a Glass Removal kit to take care of this.",,,,,,,,,,,,,

modArmorHelmetLightWeak,,,,, "Weak Helmet Light Mod",,,,,,,,,,,,,
modArmorHelmetLightWeakDesc,,,,, "A old weak light source that can be installed into any head armor. Use your Light Source action (F) to activate.",,,,,,,,,,,,,


tutorialTipQuest01,Quest,Quest Info,,, "These are The End-Times. \n\nThere was no hope of survival. \n\nThis is the story of how you died ...",,,,,,,,,,,,,
tutorialTipQuest01_title,Quest,Quest Info,,, "This is the story of how you died",,,,,,,,,,,,,
tutorialTipQuest02,Quest,Quest Info,,, "Now that you have some clothing you can make the basic tools and choose what weapon(s) to make before continuing forward. \n\nYour goal, if you so choose, is to clear each of the four (4) skyscrapers before you inevitably perish.",,,,,,,,,,,,,
tutorialTipQuest02_title,Quest,Quest Info,,, "This is the story of how you died. This is the Beginning and the End.",,,,,,,,,,,,,
quest_BasicSurvival,Quest,Quest Info,,, "The Beginning of the End",,,,,,,,,,,,,
quest_BasicSurvival1,Quest,Quest Info,,, "Cloth Yourself",,,,,,,,,,,,,
quest_BasicSurvival1_subtitle,Quest,Quest Info,,, "Make Clothing",,,,,,,,,,,,,
quest_BasicSurvival1_description,Quest,Quest Info,,, "Your backpack has all the supplies needed to make the clothing needed plus some headgear to match your biome.",,,,,,,,,,,,,
quest_BasicSurvival2,Quest,Quest Info,,, "Make Basic Tools",,,,,,,,,,,,,
quest_BasicSurvival2_subtitle,Quest,Quest Info,,, "Make Tools",,,,,,,,,,,,,
quest_BasicSurvival2_description,Quest,Quest Info,,, "Your backpack has all the supplies needed to make the basic tools.",,,,,,,,,,,,,
quest_BasicSurvival_offer,Quest,Quest Info,,, "You have gathered the needed supplies into a backpack. You see some skyscrapers in the distance. Can you make this life count?",,,,,,,,,,,,,

skyscraper01_clear,Quest,Quest Info,,, "Dishong Tower Clear (5*)",,,,,,,,,,,,,
skyscraper01_clear_subtitle,Quest,Quest Info,,, "Dishong Tower Clear",,,,,,,,,,,,,
skyscraper01_clear_description,Quest,Quest Info,,, "Big tower, lots of zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper01_clear_offer,Quest,Quest Info,,, "One of the 4 tallest building you have seen on the horizon. Can you clear it?",,,,,,,,,,,,,
skyscraper01_clear_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers. \nIf only there were other people around to share this achievement with. \nOn to the next goal before death takes you. \n\nRumor has it you can make a item to make an even harder version of this challenge.",,,,,,,,,,,,,

skyscraper02_clear,Quest,Quest Info,,, "Crack-a-Book HQ Clear (5*)",,,,,,,,,,,,,
skyscraper02_clear_subtitle,Quest,Quest Info,,, "Crack-a-Book HQ Clear",,,,,,,,,,,,,
skyscraper02_clear_description,Quest,Quest Info,,, "Big tower, lots of zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper02_clear_offer,Quest,Quest Info,,, "One of the 4 tallest building you have seen on the horizon. Can you clear it?",,,,,,,,,,,,,
skyscraper02_clear_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers. \nIf only there were other people around to share this achievement with. \nOn to the next goal before death takes you. \n\nRumor has it you can make a item to make an even harder version of this challenge.",,,,,,,,,,,,,

skyscraper03_clear,Quest,Quest Info,,, "Higashi Pharmaceutical Clear (5*)",,,,,,,,,,,,,
skyscraper03_clear_subtitle,Quest,Quest Info,,, "Higashi Pharmaceutical Clear",,,,,,,,,,,,,
skyscraper03_clear_description,Quest,Quest Info,,, "Big tower, lots of zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper03_clear_offer,Quest,Quest Info,,, "One of the 4 tallest building you have seen on the horizon. Can you clear it?",,,,,,,,,,,,,
skyscraper03_clear_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers. \nIf only there were other people around to share this achievement with. \nOn to the next goal before death takes you. \n\nRumor has it you can make a item to make an even harder version of this challenge.",,,,,,,,,,,,,

skyscraper04_clear,Quest,Quest Info,,, "Tower Construction Site Clear (4*)",,,,,,,,,,,,,
skyscraper04_clear_subtitle,Quest,Quest Info,,, "Tower Construction Site Clear",,,,,,,,,,,,,
skyscraper04_clear_description,Quest,Quest Info,,, "Big tower, lots of zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper04_clear_offer,Quest,Quest Info,,, "One of the 4 tallest building you have seen on the horizon. Can you clear it?",,,,,,,,,,,,,
skyscraper04_clear_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers. \nIf only there were other people around to share this achievement with. \nOn to the next goal before death takes you. \n\nRumor has it you can make a item to make an even harder version of this challenge.",,,,,,,,,,,,,

qt_skyscraper01_clear,items,Item,,, "To give yourself the quest to clear Dishong Tower Clear (5*) again.",,,,,,,,,,,,,
qt_skyscraper02_clear,items,Item,,, "To give yourself the quest to clear Crack-a-Book HQ Clear (5*) again.",,,,,,,,,,,,,
qt_skyscraper03_clear,items,Item,,, "To give yourself the quest to clear Higashi Pharmaceutical Clear (5*) again.",,,,,,,,,,,,,
qt_skyscraper04_clear,items,Item,,, "To give yourself the quest to clear Tower Construction Site Clear (4*) again.",,,,,,,,,,,,,



qt_skyscraper01_clear_infested,items,Item,,, "To give yourself the quest to clear Dishong Tower Infested Clear (5*I).",,,,,,,,,,,,,
qt_skyscraper02_clear_infested,items,Item,,, "To give yourself the quest to clear Crack-a-Book HQ Infested Clear (5*I).",,,,,,,,,,,,,
qt_skyscraper03_clear_infested,items,Item,,, "To give yourself the quest to clear Higashi Pharmaceutical Infested Clear (5*I).",,,,,,,,,,,,,
qt_skyscraper04_clear_infested,items,Item,,, "To give yourself the quest to clear Tower Construction Site Infested Clear (4*I).",,,,,,,,,,,,,

skyscraper01_clear_infested,Quest,Quest Info,,, "Dishong Tower Infested Clear (5*I)",,,,,,,,,,,,,
skyscraper01_clear_infested_subtitle,Quest,Quest Info,,, "Dishong Tower Infested Clear",,,,,,,,,,,,,
skyscraper01_clear_infested_description,Quest,Quest Info,,, "Big tower, lots of powerful zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper01_clear_infested_offer,Quest,Quest Info,,, "You have somehow cleared it once. Can you do it again, with stronger zombies?",,,,,,,,,,,,,
skyscraper01_clear_infested_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers INFESTED. \nMaybe you will get some peace before death comes after all.",,,,,,,,,,,,,

skyscraper02_clear_infested,Quest,Quest Info,,, "Crack-a-Book HQ Infested Clear (5*I)",,,,,,,,,,,,,
skyscraper02_clear_infested_subtitle,Quest,Quest Info,,, "Crack-a-Book HQ Infested Clear",,,,,,,,,,,,,
skyscraper02_clear_infested_description,Quest,Quest Info,,, "Big tower, lots of powerful zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper02_clear_infested_offer,Quest,Quest Info,,, "You have somehow cleared it once. Can you do it again, with stronger zombies?",,,,,,,,,,,,,
skyscraper02_clear_infested_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers INFESTED. \nMaybe you will get some peace before death comes after all.",,,,,,,,,,,,,

skyscraper03_clear_infested,Quest,Quest Info,,, "Higashi Pharmaceutical Infested Clear (5*I)",,,,,,,,,,,,,
skyscraper03_clear_infested_subtitle,Quest,Quest Info,,, "Higashi Pharmaceutical Infested Clear",,,,,,,,,,,,,
skyscraper03_clear_infested_description,Quest,Quest Info,,, "Big tower, lots of powerful zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper03_clear_infested_offer,Quest,Quest Info,,, "You have somehow cleared it once. Can you do it again, with stronger zombies?",,,,,,,,,,,,,
skyscraper03_clear_infested_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers INFESTED. \nMaybe you will get some peace before death comes after all.",,,,,,,,,,,,,

skyscraper04_clear_infested,Quest,Quest Info,,, "Tower Construction Site Infested Clear (4*I)",,,,,,,,,,,,,
skyscraper04_clear_infested_subtitle,Quest,Quest Info,,, "Tower Construction Site Infested Clear",,,,,,,,,,,,,
skyscraper04_clear_infested_description,Quest,Quest Info,,, "Big tower, lots of powerful zombies, certain death. But there may be loot and it beats dying quietly.",,,,,,,,,,,,,
skyscraper04_clear_infested_offer,Quest,Quest Info,,, "You have somehow cleared it once. Can you do it again, with stronger zombies?",,,,,,,,,,,,,
skyscraper04_clear_infested_Completion_tip,Quest,Quest Info,,, "Amazing! \nYou managed to clear out one of the skyscrapers INFESTED. \nMaybe you will get some peace before death comes after all.",,,,,,,,,,,,,


q_tier1_clear_custom,items,Item,,, "Generate a Tier 1 Clear Quest",,,,,,,,,,,,,
q_tier2_clear_custom,items,Item,,, "Generate a Tier 2 Clear Quest",,,,,,,,,,,,,
q_tier3_clear_custom,items,Item,,, "Generate a Tier 3 Clear Quest",,,,,,,,,,,,,
q_tier4_clear_custom,items,Item,,, "Generate a Tier 4 Clear Quest",,,,,,,,,,,,,
q_tier5_clear_custom,items,Item,,, "Generate a Tier 5 Clear Quest",,,,,,,,,,,,,
q_tier6_clear_custom,items,Item,,, "Generate a Tier 6 Clear Quest (Not Guaranteed to Work)",,,,,,,,,,,,,


quest_tier1_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,
quest_tier2_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,
quest_tier3_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,
quest_tier4_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,
quest_tier5_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,
quest_tier6_clear_offer,,,,, "Go clear this place out, there might be some stuff, or maybe just death.",,,,,,,,,,,,,






storageLargeVariantHelper,,,,, "AGF's Everything Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericLarge,,,,, "AGF's Everything Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericSignedInsecureLarge,,,,,"AGF's Everything Insecure Writable Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericSignedLarge,,,,,"AGF's Everything Writable Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericSignedInsecureLarge,,,,,"AGF's Everything Insecure Writable Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericLarge,,,,,"AGF's Everything Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageGenericInsecureLarge,,,,,"AGF's Everything Insecure Storage [ddcdfa](168)",,,,,,,,,,,,,
cntStorageChestLarge,,,,,"AGF's Everything Insecure Chest [ddcdfa](168)",,,,,,,,,,,,,
cntSecureStorageChestLarge,,,,,"AGF's Everything Chest [ddcdfa](168)",,,,,,,,,,,,,


YubiDanResidence,POI,POIName,,x,YubiDan Residence,,,,,,,,,,,,,
LoopOfHell,POI,POIName,,x,The Loop of Hells,,,,,,,,,,,,,
StiffJonnyTown,POI,POIName,,x,Stiff Johnny Town,,,,,,,,,,,,,

lootItem,,Pick up prompt,,,"Press [00ACFF]{0}[-] to pick up [00ACFF]{1}[-]",,"Drücke [00ACFF]{0}[-], um [00ACFF]{1}[-] aufzuheben","Pulsa [00ACFF]{0}[-] para coger [00ACFF]{1}[-]","Appuyer sur [00ACFF]{0}[-] pour ramasser : [00ACFF]{1}[-]","Premi [00ACFF]{0}[-] per raccogliere [00ACFF]{1}[-]","[00ACFF]{0}[-]を押して[00ACFF]{1}[-]を拾う","[00ACFF]{0}[-]을(를) 눌러 [00ACFF]{1}[-] 집어들기","Przyciśnij [00ACFF]{0}[-], aby podnieść [00ACFF]{1}[-]","Aperte [00ACFF]{0}[-] para pegar [00ACFF]{1}[-]","Нажмите [00ACFF]{0}[-], чтобы подобрать предмет [00ACFF]{1}[-]","[00ACFF]{1}[-] Almak için [00ACFF]{0}[-] tuşuna bas","按下[00ACFF]{0}[-]拾取[00ACFF]{1}[-]","按下 [00ACFF]{0}[-] 撿起 [00ACFF]{1}[-]"
pickupPrompt,UI,HUD,,,"Press [00ACFF]{{0}}[-] to pick up: [00ACFF]{0}[-]",,"Drücke [00ACFF]{{0}}[-], um [00ACFF]{0}[-] aufzuheben","Pulsa [00ACFF]{{0}}[-] para coger: [00ACFF]{0}[-]","Appuyer sur [00ACFF]{{0}}[-] pour ramasser : [00ACFF]{0}[-]","Premi [00ACFF]{{0}}[-] per raccogliere: [00ACFF]{0}[-]","[00ACFF]{{0}}[-] を押して拾う： [00ACFF]{0}[-]","[00ACFF]{{0}}[-]을(를) 눌러 집어들기:[00ACFF]{0}[-]","Przyciśnij [00ACFF]{{0}}[-], aby podnieść: [00ACFF]{0}[-]","Aperte [00ACFF]{{0}}[-] para pegar: [00ACFF]{0}[-]","Нажмите [00ACFF]{{0}}[-], чтобы подобрать: [00ACFF]{0}[-]","[00ACFF]{{0}}[-] tuşuna basarak al: [00ACFF]{0}[-]","按下[00ACFF]{{0}}[-]拾取：[00ACFF]{0}[-]","按 [00ACFF]{{0}}[-] 撿起[00ACFF]{0}[-]"
turretPickUp,UI,HUD,,,"[00ACFF]Pick up Turret",,"[00ACFF]Geschützturm aufheben","[00ACFF]Torreta de recogida","[00ACFF]Ramasser la tourelle","[00ACFF]Raccogli torretta","[00ACFF]砲塔を拾う","[00ACFF]터렛 집어들기","[00ACFF]Wybór wieżyczki","[00ACFF]Pegar torreta","[00ACFF]Подобрать пушку","[00ACFF]Tareti yerden al","[00ACFF]拾取炮塔","[00ACFF]撿起炮塔"



infectionTip,Journal Tip,,,,"Infection is a constant danger in the zombie apocalypse.\nInfection is not curable.\nFighting zombies is the most common source of infection and wearing armor is highly recommended.\n\nInfection will drain your stamina and in later stages of the infection also your attributes. If untreated it is lethal within 7 game days.\n\nUsing any applicable cure (Honey, Herbal Antibiotics, Antibiotics) will immediately make you feel better and reduce the negative effects. A cure reduces the infection percentage by the amount listed on the item.\n\nThe Infection symbol on the main screen will change to show if it is being treated (getting better) or not.",,,,,,,,,,,,,

spikesTip,Journal Tip,,,,"Spikes in Never Alone have had their durability greatly increased.\nThey also can cause bleed now.\nThis means you also should be careful of them as well as consider using them to battle the onslaught.",,,,,,,,,,,,,
spikesTip_title,Journal Tip,,,,"Spike Traps",,,,,,,,,,,,,

preventionTip,Journal Tip,,,,"Vitamins and Honey both provide protection against getting infected.\nVitamins also last for around 48 minutes, so you should always try to keep the buff running if possible to try not get infected.",,,,,,,,,,,,,
preventionTip_title,Journal Tip,,,,"Preventing Infection",,,,,,,,,,,,,

glassTip,Journal Tip,,,,"Glass is a danger in the environment.\nTake care to not walk over random trash as it may hurt you.\nIf you do you can make a Glass Removal Kit to remove the glass and bandage the wound.",,,,,,,,,,,,,
glassTip_title,Journal Tip,,,,"Glass and Glass Removal Kit",,,,,,,,,,,,,



ammo9mmBulletBallDismantled,,,,,"Dismantled 9mm Ball",,,,,,,,,,,,,
ammo9mmBulletHPDismantled,,,,,"Dismantled 9mm HP",,,,,,,,,,,,,
ammo9mmBulletAPDismantled,,,,,"Dismantled 9mm AP",,,,,,,,,,,,,
ammo44MagnumBulletBallDismantled,,,,,"Dismantled .44 Ball",,,,,,,,,,,,,
ammo44MagnumBulletHPDismantled,,,,,"Dismantled .44 HP",,,,,,,,,,,,,
ammo44MagnumBulletAPDismantled,,,,,"Dismantled .44 AP",,,,,,,,,,,,,
ammo762mmBulletBallDismantled,,,,,"Dismantled 7.62 Ball",,,,,,,,,,,,,
ammo762mmBulletHPDismantled,,,,,"Dismantled 7.62 HP",,,,,,,,,,,,,
ammo762mmBulletAPDismantled,,,,,"Dismantled 7.62 AP",,,,,,,,,,,,,
ammoShotgunShellDismantled,,,,,"Dismantled Shotgun Shell",,,,,,,,,,,,,
ammoShotgunSlugDismantled,,,,,"Dismantled Shotgun Slug",,,,,,,,,,,,,
ammoShotgunBreachingSlugDismantled,,,,,"Dismantled Shotgun Breaching Slug",,,,,,,,,,,,,
ammoJunkTurretRegularDismantled,,,,,"Dismantled Turret Regular",,,,,,,,,,,,,
ammoJunkTurretShellDismantled,,,,,"Dismantled Turret Shell",,,,,,,,,,,,,
ammoJunkTurretAPDismantled,,,,,"Dismantled Turret AP",,,,,,,,,,,,,
ammoRocketHEDismantled,,,,,"Dismantled Rocket HE",,,,,,,,,,,,,
ammoRocketFragDismantled,,,,,"Dismantled Rocket Frag",,,,,,,,,,,,,
ammoArrowStoneDismantled,,,,,"Dismantled Stone Arrow",,,,,,,,,,,,,
ammoArrowIronDismantled,,,,,"Dismantled Iron Arrow",,,,,,,,,,,,,
ammoArrowSteelAPDismantled,,,,,"Dismantled Steel AP Arrow",,,,,,,,,,,,,
ammoArrowFlamingDismantled,,,,,"Dismantled Flaming Arrow",,,,,,,,,,,,,
ammoArrowExplodingDismantled,,,,,"Dismantled Exploding Arrow",,,,,,,,,,,,,
ammoCrossbowBoltStoneDismantled,,,,,"Dismantled Stone Bolt",,,,,,,,,,,,,
ammoCrossbowBoltIronDismantled,,,,,"Dismantled Iron Bolt",,,,,,,,,,,,,
ammoCrossbowBoltSteelAPDismantled,,,,,"Dismantled Steel AP Bolt",,,,,,,,,,,,,
ammoCrossbowBoltFlamingDismantled,,,,,"Dismantled Flaming Bolt",,,,,,,,,,,,,
ammoCrossbowBoltExplodingDismantled,,,,,"Dismantled Exploding Bolt",,,,,,,,,,,,,

ammo9mmBulletBallDismantledDesc,,,,,"Dismantled 9mm Ball Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo9mmBulletHPDismantledDesc,,,,,"Dismantled 9mm HP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo9mmBulletAPDismantledDesc,,,,,"Dismantled 9mm AP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo44MagnumBulletBallDismantledDesc,,,,,"Dismantled .44 Ball Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo44MagnumBulletHPDismantledDesc,,,,,"Dismantled .44 HP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo44MagnumBulletAPDismantledDesc,,,,,"Dismantled .44 AP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo762mmBulletBallDismantledDesc,,,,,"Dismantled 7.62 Ball Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo762mmBulletHPDismantledDesc,,,,,"Dismantled 7.62 HP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammo762mmBulletAPDismantledDesc,,,,,"Dismantled 7.62 AP Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoShotgunShellDismantledDesc,,,,,"Dismantled Shotgun Shell Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoShotgunSlugDismantledDesc,,,,,"Dismantled Shotgun Slug Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoShotgunBreachingSlugDismantledDesc,,,,,"Dismantled Shotgun Breaching Slug Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoJunkTurretRegularDismantledDesc,,,,,"Dismantled Turret Regular Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoJunkTurretShellDismantledDesc,,,,,"Dismantled Turret Shell Parts\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoJunkTurretAPDismantledDesc,,,,,"Dismantled Turret AP Parts\Components have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoRocketHEDismantledDesc,,,,,"Dismantled Rocket HE Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoRocketFragDismantledDesc,,,,,"Dismantled Rocket Frag Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoArrowStoneDismantledDesc,,,,,"Dismantled Stone Arrow Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoArrowIronDismantledDesc,,,,,"Dismantled Iron Arrow Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoArrowSteelAPDismantledDesc,,,,,"Dismantled Steel AP Arrow Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoArrowFlamingDismantledDesc,,,,,"Dismantled Flaming Arrow Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoArrowExplodingDismantledDesc,,,,,"Dismantled Exploding Arrow Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoCrossbowBoltStoneDismantledDesc,,,,,"Dismantled Stone Bolt Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoCrossbowBoltIronDismantledDesc,,,,,"Dismantled Iron Bolt Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoCrossbowBoltSteelAPDismantledDesc,,,,,"Dismantled Steel AP Bolt Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoCrossbowBoltFlamingDismantledDesc,,,,,"Dismantled Flaming Bolt Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,
ammoCrossbowBoltExplodingDismantledDesc,,,,,"Dismantled Exploding Bolt Components\nWill have better returns if crafted from a workbench (70%) than in a backpack (47%).",,,,,,,,,,,,,


repairToolsDisp,,,,,"Repairs With",,,,,,,,,,,,,


perkHealingFactorDesc,progression,perk For,,,"Specialize in boosting your natural healing rates as long as you're not starving.\nSelf-healing will not work when you are out of food or water.\n\nDoes Not Use Food to Heal You, BUT will cost 2 points a level",,,,,,,,,,,,,

reqFortitudeLevel01HF,progression,Attribute,,,"Fortitude Level 1, 2 Points",,,,,,,,,,,,,
reqFortitudeLevel03HF,progression,Attribute,,,"Fortitude Level 3, 2 Points",,,,,,,,,,,,,
reqFortitudeLevel05HF,progression,Attribute,,,"Fortitude Level 5, 2 Points",,,,,,,,,,,,,
reqFortitudeLevel07HF,progression,Attribute,,,"Fortitude Level 7, 2 Points",,,,,,,,,,,,,
reqFortitudeLevel10HF,progression,Attribute,,,"Fortitude Level 10, 2 Points",,,,,,,,,,,,,


miningTip,Journal Tip,,,,"Different Ores can be found in different biomes and can be found at any depth. These ores are Iron, Lead, Coal and Nitrate.\nThe forest has no Coal, Lead, or Oil Shale.\nThe desert is special in that you will find Oil Shale but no Coal.\nThe wasteland will have every ore, including Oil Shale.\nOre piles are often found where ore veins exist close to the surface. Dig straight down, not sideways, to find an ore vein.\nClay Soil can be dug up almost everywhere.",,,,,,,,,,,,,


goDifficulty1,UI,Menu,x,,"Adventurer",,,,,,,,,,,,,
goDifficulty2,UI,Menu,x,,"Nomad",,,,,,,,,,,,,
goDifficulty3,UI,Menu,x,,"Warrior",,,,,,,,,,,,,
goDifficulty4,UI,Menu,x,,"Survivalist",,,,,,,,,,,,,
goDifficulty5,UI,Menu,x,,"Insane",,,,,,,,,,,,,
goDifficulty6,UI,Menu,x,,"YubiDan (Masochist)",,,,,,,,,,,,,
goDifficultyDesc,UI,Menu,x,,"Adventurer:\n   -25% Zombie Health\n   125% Zombie Damage\n\nNomad:\n    +0% Zombie Health\n   150% Zombie Damage\n\nWarrior:\n   +25% Zombie Health\n   200% Zombie Damage\n\nSurvivalist:\n   +50% Zombie Health\n   250% Zombie Damage\n\nInsane:\n   +75% Zombie Health\n   300% Zombie Damage\n\nYubiDan (Masochist):\n   +100% Zombie Health\n   350% Zombie Damage "
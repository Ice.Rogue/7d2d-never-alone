VanillaPlus - ElectricityPlus
A21 - Version 1.0.4



***Requires VP-1Core, otherwise known as Vanilla Plus Core***


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	
	-ElectricityPlus is SAFE to install on new or existing game.
		(If existing game, replace all generators with power cores.)
	-ElectricityPlus is DANGEROUS to remove from an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	-Electrical banks expanded, requiring less to be needed. Helps performance.
	-Engines, batteries, and solar cells stack together and no longer have qualities.
	-Power Cores are now used inside the electrical banks to prevent stealing and other glitches.
	-A "Chargeable" power core is used in place of batteries, crafted from power cores.
	-Solar Banks are craftable at level 85 of electrician.


______________________________________________________________________________________________________________________
5.  CHANGELOG

v1.0.4
-Removed the crafting skill level requirement in crafting power cores.

v1.0.3
-Removed the A20 localization of grease monkey.
-Updated the readme to new format.



	

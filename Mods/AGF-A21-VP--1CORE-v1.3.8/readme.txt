VanillaPlus - Core				
A21 - Version 1.3.8


***This mod is REQUIRED for most of the Vanilla Plus features, as it is the core to its functioning.***


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	
	-VanillaPlusCore is SAFE to install on new game or existing game.
	-VanillaPlusCore is DANGEROUS to remove from an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	MAIN Features:
	-Build any door with the simplified wood, iron, steel/vault, and powered variants.
	-See all your crafting levels all on one page!
	-Rebundle Bundles: Things opened from a bundle can be crafted back into one.
	-Mining Perk now also bundles clay, sand, and brass.
	-(English Only) Naming Scheme for better sorting.
	
	OTHER Features:
	-All Archery Uses Feathers: This is for simplification, also feathers can be crafted from plastic.
	-Cobblestone Variant Block also craftable directly from clay and rock.
	-The sand you get from mining very slightly increased.
	-First Aid Bandage also craftable directly from aloe cream and a bandage.
	-Fix for car salvaging issue when you do too much damage.
	-Repair blocks that were originally unrepairable (find more, let me know!)
	-Armors now show up in crafting lists without having to search for them.
	-Bundles have a color tint for additional visual differentation.
	
	ADMIN/MODDER Features:
	-Admin Tools: Block Replace Tool can replace blocks quickly.
	-Admin Tools: Books that give a set amount of xp to a player.
	-Modding Blocks: A butcher block and salvage block for harvest testing.
	

______________________________________________________________________________________________________________________
5.  CHANGELOG
v1.3.8
-Removed the misc blocks of mine from creative menu that weren't supposed to be there.

v1.3.7
-Updated recipes for alternative bandage and first aid kit to correct cloth amounts


v1.3.6
-Made SOME of the custom blocks to NOT show up in creative menu. More to Do.


v1.3.5
-Fixed a visual error on the crafting list for shotguns around level 3.


v1.3.4
-Updated all gas recipes for A21.1
	-including localization, non-modded gas bundles now at 5k, not 10k.
-Updated the ReadMe file to new format.



	

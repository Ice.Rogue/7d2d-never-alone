Backpack72
A21 - Version 1.2.9


______________________________________________________________________________________________________________________
README TABLE OF CONTENTS
1. About Author
2. Mod Philosophy
3. Installation and Removal Notes
4. Features
5. Change Log


_____________________________________________________________________________________________________________________
1.  ABOUT AUTHOR
	-My name is AuroraGiggleFairy (AGF), previously known as RilesPlus
	-Started playing 7d2d during Alpha 12
	-Started attempting to mod in Alpha 17
	-First published a mod during Alpha 18
	-Where to find:
		https://discord.gg/Vm5eyW6N4r
		https://auroragigglefairy.github.io/
		https://www.twitch.tv/AuroraGiggleFairy
		https://7daystodiemods.com/
		https://www.nexusmods.com/7daystodie

		
______________________________________________________________________________________________________________________
2.  MOD PHILOSOPHY
	-Preferably easy installation and use!
	-Goal: Enhance Vanilla Gameplay!
	-Feedback and Testing is Beneficial!
	-Detailed Notes for Individual Preference and Mod Learning!
	-Full Language Support (best I can.)
		
	"The best mods rely on community involvement."


______________________________________________________________________________________________________________________
3.  INSTALLATION and REMOVAL NOTES
	*First, if you run into any conflicts, you may contact AuroraGiggleFairy via discord: https://discord.gg/Vm5eyW6N4r
		-All questions welcome from newcombers to seasoned 7d2d people.

	-This is a server-side mod, meaning this mod can be installed on JUST the server and will work automatically for joining players.
	-Works with EAC on or off.
	-All 14 languages supported
	-BACKPACK72 is SAFE to install on new or existing games.
	-BACKPACK72 is DANGEROUS to remove from an existing game.
		-NOTE: you can swap between Backpack72 and Backpack84 safely on an existing game.


______________________________________________________________________________________________________________________
4.  FEATURES

	-72 Slot Backpack, with 2 rows of encumbrance  slots (24 slots)
	-Updated encumbrance slots, buffs, and perks. PackMule does free up all 24 slots.
	-Reduced encumbrance effect on mobility (move speed) to prevent immobility when fully encumbered and with heavy armor.
	-Lockable slots included.


______________________________________________________________________________________________________________________
5.  CHANGELOG
v1.2.9
-Updated for A21.2(b30)

v1.2.8
-Broken writeable storages are now still writeable.

v1.2.7
-Corrected a width change of the text letting you know you are comparing items. Now it fits correctly.
-Correct text of descriptions that were going beyond the background.


v1.2.6
-Due to change in A21.1, updated the column size of "unlocked-by"
-Buff info panel on the character view screen now matches width of backpack



	
